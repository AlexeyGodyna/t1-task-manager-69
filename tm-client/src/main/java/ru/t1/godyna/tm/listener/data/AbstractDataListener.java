package ru.t1.godyna.tm.listener.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.api.endpoint.IDomainEndpoint;
import ru.t1.godyna.tm.listener.AbstractListener;

@Component
@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    public IDomainEndpoint domainEndpoint;

}
