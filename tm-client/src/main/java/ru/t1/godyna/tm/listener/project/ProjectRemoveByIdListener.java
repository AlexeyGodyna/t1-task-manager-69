package ru.t1.godyna.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.godyna.tm.event.ConsoleEvent;
import ru.t1.godyna.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    private final static String NAME = "project-remove-by-id";

    @NotNull
    private final static String DESCRIPTION = "Remove project by id.";

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(getToken(), id));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
