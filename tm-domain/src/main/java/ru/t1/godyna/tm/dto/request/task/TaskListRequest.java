package ru.t1.godyna.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.request.AbstractUserRequest;
import ru.t1.godyna.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public TaskListRequest(
            @Nullable final String token,
            @Nullable final Sort sort) {
        super(token);
        this.sort = sort;
    }

}
