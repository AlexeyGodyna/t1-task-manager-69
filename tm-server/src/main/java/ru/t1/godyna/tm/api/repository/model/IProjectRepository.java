package ru.t1.godyna.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
public interface IProjectRepository extends IUserOwnedRepository<Project> {


    long countByUserId(String userId);

    boolean existsByUserIdAndId(String userId, String id);

    @Nullable
    Project findByUserIdAndId(String userId, String id);

    @Nullable
    List<Project> findAllByUserId(String userId);

    @Transactional
    void deleteByUserId(String userId);

    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    @Transactional
    void deleteAll();

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user = :userId ORDER BY :sortType")
    List<Project> findAllByUserIdWithSort(@NotNull String user, @NotNull String sortType);

}
