package ru.t1.godyna.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.model.Session;

@Repository
@Scope("prototype")
public interface ISessionRepository extends IRepository<Session> {

    @Transactional
    void deleteByUserId(String userId);

    @Nullable
    Session findByUserIdAndId(String userId, String id);

}
