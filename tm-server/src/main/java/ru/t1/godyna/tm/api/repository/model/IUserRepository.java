package ru.t1.godyna.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.model.User;

@Repository
@Scope("prototype")
public interface IUserRepository extends IRepository<User>{

    @Nullable
    User findByLogin(String login);

    @Nullable
    User findByEmail(String email);

}
