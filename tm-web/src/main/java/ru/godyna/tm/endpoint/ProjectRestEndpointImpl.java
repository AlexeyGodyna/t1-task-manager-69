package ru.godyna.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.godyna.tm.api.endpoint.IProjectRestEndpoint;
import ru.godyna.tm.model.Project;
import ru.godyna.tm.service.ProjectService;
import ru.godyna.tm.util.UserUtil;

import javax.jws.WebParam;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @Nullable
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findOneByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.addByUserId(project, UserUtil.getUserId());
        return project;
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.removeByUserId(project, UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

}
