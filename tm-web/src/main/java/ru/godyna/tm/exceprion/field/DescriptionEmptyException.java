package ru.godyna.tm.exceprion.field;

public final class DescriptionEmptyException extends AbsractFieldException {

    public DescriptionEmptyException() {
        super("Error! Description not found...");
    }

}
