package ru.godyna.tm.exceprion.field;

public class UserIdEmptyException extends AbsractFieldException {

    public UserIdEmptyException() {
        super("Error! User Id not found...");
    }

}
