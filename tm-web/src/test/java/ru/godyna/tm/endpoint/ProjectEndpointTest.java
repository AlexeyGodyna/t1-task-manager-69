package ru.godyna.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.godyna.tm.api.client.IProjectEndpointClient;
import ru.godyna.tm.marker.IntegrationCategory;
import ru.godyna.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    final static String BASE_URL = "http://localhost:8080/api/projects";

    @NotNull
    private IProjectEndpointClient projectClient = IProjectEndpointClient.projectClient(BASE_URL);

    @NotNull
    private Collection<Project> projects = new ArrayList<>();

    @NotNull
    private final Project projectOne = new Project("TestProjectOne");

    @NotNull
    private final Project projectTwo = new Project("TestProjectTwo");

    @NotNull
    private final Project projectTree = new Project("TestProjectTree");

    @Before
    public void init() {
        projectClient.save(projectOne);
        projectClient.save(projectTwo);
        projectClient.save(projectTree);
        projects = projectClient.findAll();
    }

    @After
    public void clean() {
        projectClient.delete(projectOne);
        projectClient.delete(projectTwo);
        projectClient.delete(projectTree);
    }

    @Test
    public void findById() {
        Project project = projectClient.findById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectOne.getId());
    }

    @Test
    public void findAll() {
        Collection<Project> findProjects = projectClient.findAll();
        Assert.assertEquals(projects.size(), findProjects.size());
    }

    @Test
    public void save() {
        @NotNull final Project project = new Project("TestProjectFour");
        projectClient.save(project);
        String newProjectId = project.getId();
        Project findedProject = projectClient.findById(newProjectId);
        Assert.assertNotNull(findedProject);
        projects.add(project);
    }

    @Test
    public void delete() {
        Random random = new Random();
        int randIndex = random.nextInt(projectClient.findAll().size() - 1);
        ArrayList<Project> testProjects = (ArrayList<Project>) projectClient.findAll();
        Project randProject = testProjects.get(randIndex);
        String randProjectId = randProject.getId();
        projectClient.delete(randProject);
        Assert.assertNull(projectClient.findById(randProjectId));
        projects.remove(randProject);
    }

    @Test
    public void deleteById() {
        Random random = new Random();
        int randIndex = random.nextInt(projectClient.findAll().size() - 1);
        ArrayList<Project> testProjects = (ArrayList<Project>) projectClient.findAll();
        Project randProject = testProjects.get(randIndex);
        String randProjectId = randProject.getId();
        projectClient.delete(randProjectId);
        Assert.assertNull(projectClient.findById(randProjectId));
        projects.remove(randProject);
    }

}
